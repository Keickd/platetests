test("General test", function (assert) {
    assert.equal(isValidPlate("0948DNM"), true, "NORMAL");
    assert.equal(isValidPlate("1234567"), false, "ALL NUMBERS");
    assert.equal(isValidPlate("BCDFGHJ"), false, "ALL LETTERS");
    assert.equal(isValidPlate("B454BFD"), false, "INCORRECT LETTER POSITION");
    assert.equal(isValidPlate("4454B8D"), false, "INCORRECT NUMBER POSITION");
});

test("No acepted symbols", function(assert){
    assert.equal(isValidPlate("9999   "), false, "SPACES DETECTED AT THE END");
    assert.equal(isValidPlate("    LKP"), false, "SPACES DETECTED AT THE BEGGINING");
});

test("Lowercase data", function(assert){
    assert.equal(isValidPlate("9999ghf"), true, "LOWERCASE LETTERS");
    assert.equal(isValidPlate("ghjklrt"), false, "ALL LOWERCASE LETTERS");
});

test("No accepted value", function(assert){
    assert.equal(isValidPlate("9949AZZ"), false, "INCORRECT LETTER");
    assert.equal(isValidPlate("9949EZZ"), false, "INCORRECT LETTER");
    assert.equal(isValidPlate("9949IZZ"), false, "INCORRECT LETTER");
    assert.equal(isValidPlate("9949OZZ"), false, "INCORRECT LETTER");
    assert.equal(isValidPlate("9949UZZ"), false, "INCORRECT LETTER");
    assert.equal(isValidPlate("9949ÑZZ"), false, "INCORRECT LETTER");
    assert.equal(isValidPlate("9949QZZ"), false, "INCORRECT LETTER");
});

test("No accepted value, bad position", function(assert){
    assert.equal(isValidPlate("A949GZZ"), false, "INCORRECT LETTER");
    assert.equal(isValidPlate("E949GZZ"), false, "INCORRECT LETTER");
    assert.equal(isValidPlate("I949HZZ"), false, "INCORRECT LETTER");
    assert.equal(isValidPlate("O949KZZ"), false, "INCORRECT LETTER");
    assert.equal(isValidPlate("U499FZZ"), false, "INCORRECT LETTER");
    assert.equal(isValidPlate("Ñ949LZZ"), false, "INCORRECT LETTER");
    assert.equal(isValidPlate("Q949LZZ"), false, "INCORRECT LETTER");
});

test("sizes", function(assert){
    assert.equal(isValidPlate("49495GZZ"), false, "MAX 7 WITH NUMBERS"); /*This shouldn't happen*/ 
    assert.equal(isValidPlate("4949GZZF"), false, "MAX 7 WITH LETTERS"); /*This shouldn't happen*/ 
    assert.equal(isValidPlate("499GZZ"), false, "LESS 7 WITH NUMBERS");
    assert.equal(isValidPlate("4599GZ"), false, "LESS 7 WITH LETTERS");
});
